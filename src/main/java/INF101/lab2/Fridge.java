package INF101.lab2;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

public class Fridge implements IFridge {
	
	private List<FridgeItem> items = new ArrayList<>();
	
	private final Integer maxsize = 20;
	
	@Override
	public int nItemsInFridge() {
		// TODO Auto-generated method stub
		
		return this.items.size();
	}

	@Override
	public int totalSize() {
		// TODO Auto-generated method stub
		return this.maxsize;
	}

	@Override
	public boolean placeIn(FridgeItem item) {
		// TODO Auto-generated method stub
		
		if(this.items.size()<this.maxsize) {
			this.items.add(item);
			return true;
		}else {
			return false;
		}
		 
	}

	@Override
	public void takeOut(FridgeItem item) {
		// TODO Auto-generated method stub
		
		if(this.items.contains(item)) {
			this.items.remove(item);
		}else {
			throw new NoSuchElementException();
		}
	
	}

	@Override
	public void emptyFridge() {
		// TODO Auto-generated method stub
		this.items.clear();
		
	}

	@Override
	public List<FridgeItem> removeExpiredFood() {
		// TODO Auto-generated method stub
		List<FridgeItem> expiredItems = new ArrayList<>();
		
		for(FridgeItem item: this.items) {
			try{
				if(item.hasExpired()){
					expiredItems.add(item);
				}else {
					continue;
				}
			}catch(NoSuchElementException e){
				System.out.println(e);
			}

		}
		for(FridgeItem item : expiredItems) {
			this.takeOut(item);
		}
		return expiredItems;
	}

}
